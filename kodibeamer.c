#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <usb.h>
#include "sispm_ctl.h"

#define BAUDRATE	B9600
#define DEVICE		"/dev/ttyS0"
#define MAXLINE		1024
#define QUEUELEN	64

#define BUFLEN		512
#define PORT		8888

#define ANS_OFF		"Switching off\n"
#define ANS_AUDIO	"Switching to Audio mode\n"
#define ANS_VIDEO	"Switching to Video mode\n"
#define ANS_UNKN	"Unknown command: '"

#define STATE_POFF		0x10
#define STATE_BOOT		0x11
#define STATE_PON		0x12
#define STATE_SHUT		0x13
#define STATE_UNKN		0x19

#define ROOM_OFF		0x30
#define ROOM_AUDIO		0x31
#define ROOM_BOOT		0x32
#define ROOM_VIDEO		0x33
#define ROOM_SHUT		0x34
#define ROOM_UNKN		0x39

#define QUERY_POWER		0x20
#define QUERY_INPUT		0x21
#define QUERY_MUTE		0x22
#define QUERY_LAMP		0x23
#define QUERY_UNKN		0x29

#define SOURCE_RGB		0xA0
#define SOURCE_RGB2		0xA1
#define SOURCE_YUV		0xA2
#define SOURCE_DVIA		0xA3
#define SOURCE_DVID		0xA4
#define SOURCE_CBAS		0xA5
#define SOURCE_YC		0xA6
#define SOURCE_HDMI		0xA7
#define SOURCE_HDMI2	0xA8
#define SOURCE_UNKN		0xAF

struct cmdQueueType {
	struct cmdQueueType *next;
	char cmd[QUEUELEN];
	int query;
};

int doQuery, nextQuery;
int serState, beamState, lastQuery, resetQueryCounter;
int selSrc = SOURCE_HDMI, actSrc;
char inLine[MAXLINE];
struct cmdQueueType *cmdQueue = NULL;
char inChar;
int lptr;
int	writeLock;

int verbose = 1;

void die(char *s) {
	perror (s);
	exit(1);
}

void daemonize() {
  pid_t pid, sid;

  pid = fork();
  if (pid < 0)
    die("first fork");
  if (pid > 0)
    exit(0);
  umask(0);
  sid = setsid();
  if (sid < 0)
    die("setsid");

  if ((chdir("/var/tmp")) < 0)
    die("setsid");

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
}


void queueCmd(char *toQueue, int type) {
	struct cmdQueueType *ptr, *last;

	//printf("Sending out: %s\n", toQueue);
	last = NULL;
	ptr = cmdQueue;
	while ( ptr != NULL ) {
		last = ptr;
		ptr = ptr->next;
	}
	ptr = malloc(sizeof(struct cmdQueueType));
	if ( ptr == NULL )
		die("Could not allocate queue memory");
	ptr->next = NULL;
	ptr->query = type;
	strncpy(ptr->cmd, toQueue, QUEUELEN);
	if ( last == NULL )
		cmdQueue = ptr;
	else
		last->next = ptr;
}

void handleQueue(int serial) {
	struct cmdQueueType *oldptr;
	if (! writeLock && cmdQueue != NULL ) {
		writeLock = 1;
		lastQuery = cmdQueue->query;
		write(serial, "\r*", 2);
		write(serial, cmdQueue->cmd, strlen(cmdQueue->cmd));
		write(serial, "#\r", 2);
		oldptr = cmdQueue;
		cmdQueue = cmdQueue->next;
		free(oldptr);
		usleep(100000);
		writeLock = 0;
	}
}

void handleInput() {
	if (!strcmp(inLine, "POW=ON#")) {
		resetQueryCounter = 10;
		switch (lastQuery) {
			case QUERY_POWER:
				beamState = STATE_PON;
				nextQuery = QUERY_INPUT;
				break;
			case STATE_PON:
				beamState = STATE_BOOT;
				nextQuery = QUERY_POWER;
				break;
			default:
				printf("Power is now ON\n");
				nextQuery = QUERY_POWER;
				break;
		}
	} else if (!strcmp(inLine, "POW=OFF#")) {
		resetQueryCounter = 10;
		switch (lastQuery) {
			case QUERY_POWER:
				beamState = STATE_POFF;
				nextQuery = QUERY_POWER;
				break;
			case STATE_POFF:
				beamState = STATE_SHUT;
				nextQuery = QUERY_POWER;
				break;
			default:
				printf("Power is now OFF\n");
				nextQuery = QUERY_POWER;
				break;
		}
	} else if (!strcmp(inLine, "Block item#")) {
		// Turning power on or off
		resetQueryCounter--;
	} else if (!strncmp(inLine, "SOUR=", 5)) {
		resetQueryCounter = 10;
		nextQuery = QUERY_MUTE;
		if (!strcmp(inLine, "SOUR=RGB#")) {
			actSrc = SOURCE_RGB;
		} else if (!strcmp(inLine, "SOUR=RGB2#")) {
			actSrc = SOURCE_RGB2;
		} else if (!strcmp(inLine, "SOUR=YPBR#")) {
			actSrc = SOURCE_YUV;
		} else if (!strcmp(inLine, "SOUR=DVID#")) {
			actSrc = SOURCE_DVID;
		} else if (!strcmp(inLine, "SOUR=DVIA#")) {
			actSrc = SOURCE_DVIA;
		} else if (!strcmp(inLine, "SOUR=HDMI#")) {
			actSrc = SOURCE_HDMI;
		} else if (!strcmp(inLine, "SOUR=HDMI2#")) {
			actSrc = SOURCE_HDMI2;
		} else if (!strcmp(inLine, "SOUR=VID#")) {
			actSrc = SOURCE_CBAS;
		} else if (!strcmp(inLine, "SOUR=SVID#")) {
			actSrc = SOURCE_YC;
		}
		if ( actSrc != selSrc ) {
		    usleep(10000);
			switch (selSrc) {
				case SOURCE_RGB:
					queueCmd("sour=rgb", QUERY_UNKN);
					break;
				case SOURCE_RGB2:
					queueCmd("sour=rgb2", QUERY_UNKN);
					break;
				case SOURCE_YUV:
					queueCmd("sour=ypbr", QUERY_UNKN);
					break;
				case SOURCE_DVID:
					queueCmd("sour=dvid", QUERY_UNKN);
					break;
				case SOURCE_DVIA:
					queueCmd("sour=dvia", QUERY_UNKN);
					break;
				case SOURCE_HDMI:
					queueCmd("sour=hdmi", QUERY_UNKN);
					break;
				case SOURCE_HDMI2:
					queueCmd("sour=hdmi2", QUERY_UNKN);
					break;
				case SOURCE_CBAS:
					queueCmd("sour=vid", QUERY_UNKN);
					break;
				case SOURCE_YC:
					queueCmd("sour=svid", QUERY_UNKN);
					break;
			}
		    sleep(1);
		}
	} else if (!strncmp(inLine, "BLANK=", 6)) {
		resetQueryCounter = 10;
		nextQuery = QUERY_LAMP;
	} else if (!strncmp(inLine, "LTIM=", 5)) {
		resetQueryCounter = 10;
		nextQuery = QUERY_POWER;
	} else {
		printf("Received unintelligible answer from Beamer: %s\n", inLine);
		resetQueryCounter--;
	}
	if ( ! resetQueryCounter )
		nextQuery = QUERY_POWER;
}

void readbeamer(int serial) {
	while ( read(serial, &inChar, 1) > 0 ) {
		switch (serState) {
			case STATE_POFF:
				if (inChar == '*') {
					serState = STATE_BOOT;
					lptr = 0;
				} else {
					serState = STATE_SHUT;
				}
				break;
			case STATE_BOOT:
				if (lptr < MAXLINE)
					inLine[lptr++] = inChar;
				if (inChar == '#' || inChar == '\r' || inChar == '\n' || lptr == MAXLINE) {
					inLine[lptr] = '\0';
					handleInput();
					serState = STATE_SHUT;
				}
				break;
			case STATE_SHUT:
				if (inChar == '\r' || inChar == '\n') {
					serState = STATE_POFF;
				}
				break;
			default:
				serState = STATE_SHUT;
		}
	}
}

void switchPower(struct usb_device *usbdev, int which, int state) {
	//printf("Trying to switch power of outlet #%i to %i\n", which, state);
	usb_dev_handle *udev = get_handle(usbdev);
	unsigned int id = get_id(usbdev);

	if ( which >= 0 && which <= 4 ) {
		switch (state) {
			case STATE_PON:
				sispm_switch_on(udev,id,which);
				break;
			case STATE_POFF:
				sispm_switch_off(udev,id,which);
				break;
			default:
				printf("Unknown switchstate %i for Outlet #%i\n", state, which);
				break;
		}
	} else 
		printf("Cannot switch Outlet #%i\n", which);

	usb_close(udev);
}

int turnPowerOff(struct usb_device *usbdev) {
	switchPower(usbdev, 2, STATE_POFF);
	switchPower(usbdev, 3, STATE_POFF);
	switch (beamState) {
		case STATE_POFF:
			switchPower(usbdev, 1, STATE_POFF);
			doQuery = 0;
			return ROOM_OFF;
		case STATE_BOOT:
			doQuery = 1;
			return ROOM_BOOT;
		case STATE_SHUT:
			doQuery = 1;
			return ROOM_SHUT;
		case STATE_PON:
			queueCmd("pow=off", STATE_POFF);
			doQuery = 1;
			return ROOM_SHUT;
	}
	return ROOM_UNKN;
}

int turnAudioOn(struct usb_device *usbdev) {
	switchPower(usbdev, 2, STATE_PON);
	switchPower(usbdev, 3, STATE_PON);
	switch (beamState) {
		case STATE_POFF:
			switchPower(usbdev, 1, STATE_POFF);
			doQuery = 0;
			return ROOM_AUDIO;
		case STATE_BOOT:
			sleep (1);
			doQuery = 1;
			return ROOM_BOOT;
		case STATE_SHUT:
			sleep (1);
			doQuery = 1;
			return ROOM_SHUT;
		case STATE_PON:
			queueCmd("pow=off", STATE_POFF);
			doQuery = 1;
			return ROOM_SHUT;
	}
	return ROOM_UNKN;
}

int turnVideoOn(struct usb_device *usbdev) {
	switchPower(usbdev, 1, STATE_PON);
	switchPower(usbdev, 2, STATE_PON);
	switchPower(usbdev, 3, STATE_PON);
	doQuery = 1;
	switch (beamState) {
		case STATE_POFF:
			doQuery = 1;
			queueCmd("pow=on", STATE_PON);
			return ROOM_BOOT;
		case STATE_BOOT:
			doQuery = 1;
			return ROOM_BOOT;
		case STATE_SHUT:
			doQuery = 1;
			return ROOM_SHUT;
		case STATE_PON:
			doQuery = 1;
			return ROOM_VIDEO;
	}
	return ROOM_UNKN;
}

int main (int ac, char **av) {

	int serial;
	struct termios serialSettings;
	struct sockaddr_in6 si_me, si_other;

	int sock, recv_len;
	unsigned int slen = sizeof(si_other);
	char udpbuf[BUFLEN];
	char *c;
	//char straddr[INET6_ADDRSTRLEN];
	int roomState = ROOM_OFF, shouldState = ROOM_UNKN;

	struct usb_bus *usbbus;
	struct usb_device *usbdev, *usbsearchdev;
	//usb_dev_handle *sudev = NULL;
	//unsigned int suid;
	//char *usbdevsn;
	
	/* Initalize Globals */
	serState = STATE_SHUT;
	beamState = STATE_UNKN;
	lastQuery = QUERY_UNKN;
	resetQueryCounter = 10;
	nextQuery = QUERY_POWER;
	doQuery = 1;
	lptr = 0;
	cmdQueue = NULL;
	writeLock = 0;

	/* Initalize TTY */
	serial = open(DEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if ( serial < 0 )
		die(DEVICE);

	if (tcgetattr(serial, &serialSettings) != 0)
		die("Getting Serial Settings");
	cfsetospeed(&serialSettings, BAUDRATE);
	cfsetispeed(&serialSettings, BAUDRATE);
	serialSettings.c_cflag = ( serialSettings.c_cflag & ~CSIZE ) | CS8 | CLOCAL | CREAD;
	serialSettings.c_cflag &= ~(PARENB | PARODD | CSTOPB | CRTSCTS);
	serialSettings.c_iflag &= ~(IGNPAR | IGNBRK | IXON | IXOFF | IXANY | INLCR);
	serialSettings.c_oflag = 0;
	serialSettings.c_lflag = 0;
	serialSettings.c_cc[VMIN] = 0;
	serialSettings.c_cc[VTIME] = 5;

	tcsetattr(serial, TCSANOW, &serialSettings);

	/* Initalize UDP Socket */
	if ((sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		die("socket");
    if ((fcntl(sock, F_SETFL, O_NONBLOCK)) == -1)
		die("non-blocking");
	memset((char *) &si_me, 0, sizeof(si_me));
	memset((char *) &udpbuf, 0, BUFLEN);
	si_me.sin6_family = AF_INET6;
	si_me.sin6_port = htons(PORT);
	si_me.sin6_addr = in6addr_any;
	if (bind(sock, (struct sockaddr*) &si_me, sizeof(si_me) ) == -1)
		die("bind");

    /* Initialize USB */
	usb_init();
	usb_find_busses();
	usb_find_devices();
	usbdev = NULL;
	for ( usbbus = usb_busses; usbbus; usbbus = usbbus->next) {
    	for (usbsearchdev = usbbus->devices; usbsearchdev; usbsearchdev = usbsearchdev->next) {
      		if ((usbsearchdev->descriptor.idVendor == VENDOR_ID)
          		&& ((usbsearchdev->descriptor.idProduct == PRODUCT_ID_SISPM) ||
              		(usbsearchdev->descriptor.idProduct == PRODUCT_ID_SISPM_FLASH_NEW))) {
				// Found it
				usbdev = usbsearchdev;
			}
		}
	}
	if ( ! usbdev )
		die("Could not find USB device");

	/*
	sudev = get_handle(usbdev);
	suid = get_id(usbdev);
	if ( ! sudev )
		die("get_handle");
	usbdevsn = strdup(get_serial(sudev));
	printf("Found USB Device with S/N %s\n", usbdevsn);
    usb_close(sudev);
	*/

	daemonize();

	while ( 1 ) {
		// Query Beamer
		if ( resetQueryCounter == 0 )
			nextQuery = QUERY_POWER;
		if ( doQuery ) {
			switch (nextQuery) {
				case QUERY_POWER:
					queueCmd("pow=?", QUERY_POWER);
					break;
				case QUERY_INPUT:
					queueCmd("sour=?", QUERY_INPUT);
					break;
				case QUERY_MUTE:
					queueCmd("blank=?", QUERY_MUTE);
					break;
				case QUERY_LAMP:
					queueCmd("ltim=?", QUERY_LAMP);
					break;
			}
		}
		handleQueue(serial);
		readbeamer(serial);

		// Handle commands
		recv_len = recvfrom(sock, udpbuf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen);
		if ( recv_len == -1 && errno != EWOULDBLOCK && errno != EAGAIN )
			die("recvfrom()");
		if ( recv_len > 0 ) {
			c = udpbuf;
			while ( *c != '\n' && ++c < udpbuf + BUFLEN );
			*c = '\0';
			//printf("Received: %s\n", udpbuf);
			if (!strcmp(udpbuf, "off")) {
				sendto(sock, ANS_OFF, strlen(ANS_OFF), 0, (struct sockaddr *) &si_other, slen);
				shouldState = ROOM_OFF;
			} else if (!strcmp(udpbuf, "video")) {
				sendto(sock, ANS_VIDEO, strlen(ANS_VIDEO), 0, (struct sockaddr *) &si_other, slen);
				shouldState = ROOM_VIDEO;
			} else if (!strcmp(udpbuf, "audio")) {
				sendto(sock, ANS_AUDIO, strlen(ANS_AUDIO), 0, (struct sockaddr *) &si_other, slen);
				shouldState = ROOM_AUDIO;
			} else {
				sendto(sock, ANS_UNKN, strlen(ANS_UNKN), 0, (struct sockaddr *) &si_other, slen);
				if ( strlen(udpbuf) > 0 )
					sendto(sock, udpbuf, strlen(udpbuf), 0, (struct sockaddr *) &si_other, slen);
				sendto(sock, "'\n", 2, 0, (struct sockaddr *) &si_other, slen);
			}
		}

		// Magic
		if ( roomState != shouldState && shouldState != ROOM_UNKN ) {
			if ( shouldState == ROOM_OFF )
				roomState = turnPowerOff(usbdev);
			if ( shouldState == ROOM_AUDIO )
				roomState = turnAudioOn(usbdev);
			if ( shouldState == ROOM_VIDEO )
				roomState = turnVideoOn(usbdev);
		}
		usleep(10000);
	}
}

