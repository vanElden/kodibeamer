# This is a Makefile

CC=gcc
CFLAGS=-Wall
LDFLAGS=-O2 -lusb
LIBS=libsispmctl.a

kodibeamer: kodibeamer.c
	$(CC) $(CFLAGS) $(LDFLAGS) kodibeamer.c $(LIBS) -o kodibeamer

clean:
	rm kodibeamer
